package controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    @DisplayName("Add two number")
    void add() {
        assertEquals(4, Calculator.add(2, 2));
        assertAll(
                () -> assertEquals(5,Calculator.add(3,2)),
        () -> assertEquals(6,Calculator.add(3,3)),

        () -> assertEquals(10,Calculator.add(5,4))

        );
    }

    @Test
    @DisplayName("Multiply two numbers")
    void multiply() {
        assertAll(() -> assertEquals(4, Calculator.multiply(2, 2)),
                () -> assertEquals(-4, Calculator.multiply(2, -2)),
                () -> assertEquals(4, Calculator.multiply(-2, -2)),
                () -> assertEquals(0, Calculator.multiply(1, 0)));
    }
}