package controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jdk.jfr.Name;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TestLoginTest {

    @Mock
    private ServletConfig config;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    HttpSession session;

    @Mock
    RequestDispatcher rd;
    @InjectMocks
    private TestLogin underTest;
    @Name("Success")
    @Test
    void login1() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1629js");
        when(request.getParameter("password")).thenReturn("Vinh21092002");
        when(request.getParameter("enterCaptcha2")).thenReturn("123ABC");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(rd);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        // Verify the session attribute value
        verify(session).setAttribute("username", "se1629js");
        verify(rd).forward(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Login successfull", result);
    }
    @Name("invalid")
    @Test
    void login2() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1629js");
        when(request.getParameter("password")).thenReturn("Vinh21092002");
        when(request.getParameter("enterCaptcha2")).thenReturn("ABCXYZ");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(rd);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        verify(rd).forward(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Invalid username, password or miss captcha", result);
    }
    @Name("fill")
    @Test
    void login3() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1629js");
        when(request.getParameter("password")).thenReturn("Vinh21092002");
        when(request.getParameter("enterCaptcha2")).thenReturn("");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }

    @Name("invalid")
    @Test
    void login4() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1629js");
        when(request.getParameter("password")).thenReturn("abc12345");
        when(request.getParameter("enterCaptcha2")).thenReturn("123ABC");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(rd);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        verify(rd).forward(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Invalid username, password or miss captcha", result);
    }
    @Name("invalid")
    @Test
    void login5() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1629js");
        when(request.getParameter("password")).thenReturn("abc12345");
        when(request.getParameter("enterCaptcha2")).thenReturn("ABCXYZ");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(rd);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        verify(rd).forward(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Invalid username, password or miss captcha", result);
    }
    @Name("fill")
    @Test
    void login6() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1629js");
        when(request.getParameter("password")).thenReturn("abc12345");
        when(request.getParameter("enterCaptcha2")).thenReturn("");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login7() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1629js");
        when(request.getParameter("password")).thenReturn("");
        when(request.getParameter("enterCaptcha2")).thenReturn("123ABC");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login8() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1629js");
        when(request.getParameter("password")).thenReturn("");
        when(request.getParameter("enterCaptcha2")).thenReturn("ABCXYZ");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login9() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1629js");
        when(request.getParameter("password")).thenReturn("");
        when(request.getParameter("enterCaptcha2")).thenReturn("");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("invalid")
    @Test
    void login10() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1630js");
        when(request.getParameter("password")).thenReturn("Vinh21092002");
        when(request.getParameter("enterCaptcha2")).thenReturn("ABCXYZ");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(rd);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        verify(rd).forward(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Invalid username, password or miss captcha", result);
    }
    @Name("invalid")
    @Test
    void login11() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1630js");
        when(request.getParameter("password")).thenReturn("Vinh21092002");
        when(request.getParameter("enterCaptcha2")).thenReturn("ABCXYZ");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(rd);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        verify(rd).forward(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Invalid username, password or miss captcha", result);
    }
    @Name("fill")
    @Test
    void login12() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1630js");
        when(request.getParameter("password")).thenReturn("Vinh21092002");
        when(request.getParameter("enterCaptcha2")).thenReturn("");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("invalid")
    @Test
    void login13() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1630js");
        when(request.getParameter("password")).thenReturn("abc1234");
        when(request.getParameter("enterCaptcha2")).thenReturn("123ABC");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(rd);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        verify(rd).forward(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Invalid username, password or miss captcha", result);
    }
    @Name("invalid")
    @Test
    void login14() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1630js");
        when(request.getParameter("password")).thenReturn("abc12345");
        when(request.getParameter("enterCaptcha2")).thenReturn("ABCXYZ");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(rd);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        verify(rd).forward(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Invalid username, password or miss captcha", result);
    }
    @Name("fill")
    @Test
    void login15() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1630js");
        when(request.getParameter("password")).thenReturn("abc12345");
        when(request.getParameter("enterCaptcha2")).thenReturn("");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login16() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1630js");
        when(request.getParameter("password")).thenReturn("");
        when(request.getParameter("enterCaptcha2")).thenReturn("");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login17() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1630js");
        when(request.getParameter("password")).thenReturn("");
        when(request.getParameter("enterCaptcha2")).thenReturn("ABCXYZ");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login18() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("se1630js");
        when(request.getParameter("password")).thenReturn("");
        when(request.getParameter("enterCaptcha2")).thenReturn("");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login19() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("");
        when(request.getParameter("password")).thenReturn("Vinh21092002");
        when(request.getParameter("enterCaptcha2")).thenReturn("123ABC");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login20() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("");
        when(request.getParameter("password")).thenReturn("Vinh21092002");
        when(request.getParameter("enterCaptcha2")).thenReturn("ABCXYZ");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login21() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("");
        when(request.getParameter("password")).thenReturn("Vinh21092002");
        when(request.getParameter("enterCaptcha2")).thenReturn("");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login22() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("");
        when(request.getParameter("password")).thenReturn("abc12345");
        when(request.getParameter("enterCaptcha2")).thenReturn("123ABC");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login23() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("");
        when(request.getParameter("password")).thenReturn("abc12345");
        when(request.getParameter("enterCaptcha2")).thenReturn("ABCXYZ");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login24() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("");
        when(request.getParameter("password")).thenReturn("abc12345");
        when(request.getParameter("enterCaptcha2")).thenReturn("");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login25() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("");
        when(request.getParameter("password")).thenReturn("");
        when(request.getParameter("enterCaptcha2")).thenReturn("123ABC");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login26() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("");
        when(request.getParameter("password")).thenReturn("");
        when(request.getParameter("enterCaptcha2")).thenReturn("ABCXYZ");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Name("fill")
    @Test
    void login27() throws IOException, ServletException {
        when(request.getParameter("username")).thenReturn("");
        when(request.getParameter("password")).thenReturn("");
        when(request.getParameter("enterCaptcha2")).thenReturn("");
        when(request.getParameter("getCaptcha2")).thenReturn("123ABC");
        when(request.getSession()).thenReturn(session);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        when(response.getWriter()).thenReturn(pw);
        new TestLogin().doPost(request, response);
        String result = sw.getBuffer().toString().trim();
        System.out.println("Result: " + result);
        assertEquals("Please fill out this field", result);
    }
    @Nested
    class WhenDoingPost {


        @BeforeEach
        void setup() {
        }
    }
}