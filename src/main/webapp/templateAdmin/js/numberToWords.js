    function onlyNumbers(evt) {
    var e = event || evt; // For trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
}

    function NumToWord(inputNumber, outputControl) {
    var str = new String(inputNumber)
    var splt = str.split("");
    var rev = splt.reverse();
    var once = ['Không', ' Một', ' Hai', ' Ba', ' Bốn', ' Năm', ' Sáu', ' Bảy', ' Tám', ' Chín'];
    var twos = ['Mười', ' Mười một', ' Mười hai', ' Mười ba', ' Mười bốn', ' Mười lăm', ' Mười sáu', ' Mười bảy', ' Mười tám', ' Mười chín'];
    var tens = ['', 'Mười', ' Hai mươi', ' Ba mươi', ' Bốn mươi', ' Năm mươi', ' Sáu mươi', ' Bảy mươi', ' Tám mươi', ' Chín mươi'];

    numLength = rev.length;
    var word = new Array();
    var j = 0;

    for (i = 0; i < numLength; i++) {
    switch (i) {

    case 0:
    if ((rev[i] == 0) || (rev[i + 1] == 1)) {
    word[j] = '';
}
    else {
    word[j] = '' + once[rev[i]];
}
    word[j] = word[j];
    break;

    case 1:
    aboveTens();
    break;

    case 2:
    if (rev[i] == 0) {
    word[j] = '';
}
    else if ((rev[i - 1] == 0) || (rev[i - 2] == 0)) {
    word[j] = once[rev[i]] + " trăm ";
}
    else {
    word[j] = once[rev[i]] + " trăm ";
}
    break;

    case 3:
    if (rev[i] == 0 || rev[i + 1] == 1) {
    word[j] = '';
}
    else {
    word[j] = once[rev[i]];
}
    if ((rev[i + 1] != 0) || (rev[i] > 0)) {
    word[j] = word[j] + "  nghìn";
}
    break;


    case 4:
    aboveTens();
    break;

    case 5:
    if ((rev[i] == 0) || (rev[i + 1] == 1)) {
    word[j] = '';
}
    else {
    word[j] = once[rev[i]];
}
    if (rev[i + 1] !== '0' || rev[i] > '0') {
    word[j] = word[j] + " trăm ";
}

    break;

    case 6:
    aboveTens();
    break;

    case 7:
    if ((rev[i] == 0) || (rev[i + 1] == 1)) {
    word[j] = '';
}
    else {
    word[j] = once[rev[i]];
}
    if (rev[i + 1] !== '0' || rev[i] > '0') {
    word[j] = word[j] + " trăm triệu";
}
    break;

    case 8:
    aboveTens();
    break;

    //            This is optional.

    //            case 9:
    //                if ((rev[i] == 0) || (rev[i + 1] == 1)) {
    //                    word[j] = '';
    //                }
    //                else {
    //                    word[j] = once[rev[i]];
    //                }
    //                if (rev[i + 1] !== '0' || rev[i] > '0') {
    //                    word[j] = word[j] + " Arab";
    //                }
    //                break;

    //            case 10:
    //                aboveTens();
    //                break;

    default: break;
}
    j++;
}

    function aboveTens() {
    if (rev[i] == 0) { word[j] = ''; }
    else if (rev[i] == 1) { word[j] = twos[rev[i - 1]]; }
    else { word[j] = tens[rev[i]]; }
}

    word.reverse();
    var finalOutput = '';
    for (i = 0; i < numLength; i++) {
    finalOutput = finalOutput + word[i];
}
    document.getElementById(outputControl).innerHTML = finalOutput;
}
