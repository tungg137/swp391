var inputColumn2 = document.getElementById("column2Search");
var inputColumn3 = document.getElementById("column3Search");
var inputColumn4 = document.getElementById("column4Search");
var inputColumn5 = document.getElementById("column5Search");
var rows = document.getElementsByTagName("tr");

// Add event listeners to the input fields
inputColumn2.addEventListener("keyup", function() {
    filterTable(inputColumn2, 1);
});
inputColumn3.addEventListener("keyup", function() {
    filterTable(inputColumn3, 2);
});
inputColumn4.addEventListener("keyup", function() {
    filterTable(inputColumn4, 3);
});
inputColumn5.addEventListener("keyup", function() {
    filterTable(inputColumn5, 4);
});

// Function to filter the table rows based on the search term
function filterTable(input, columnIndex) {
    var filter = input.value.toUpperCase();
    for (var i = 0; i < rows.length; i++) {
        var td = rows[i].getElementsByTagName("td")[columnIndex];
        if (td) {
            var text = td.textContent || td.innerText;
            if (text.toUpperCase().indexOf(filter) > -1) {
                rows[i].classList.remove("hide");
            } else {
                rows[i].classList.add("hide");
            }
        }
    }
}
