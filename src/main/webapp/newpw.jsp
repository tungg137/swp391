<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 1/17/2023
  Time: 4:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>test</title>
    <%--css--%>
    <link rel="stylesheet" href="templateAdmin/css/index.css">
    <script src="https://www.google.com/recaptcha/api.js"></script>

</head>
<body>


<div class="container" id="container">

    <div class="form-container register-container">
        <form action="DangNhap" method="post">

            <input name="username" id="username" type="text" placeholder="username" value="${object}" readonly>

            <input name="password" id="password" type="password" placeholder="password" required
                   pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                   title="Ít nhất 8 kí tự bao gồm chữ hoa, chữ thường, số">

            <!--captcha-->
            <div class="row">
                <div class="g-recaptcha"
                     data-sitekey="6Lfp3gkkAAAAAA9sGm4QNzkc0pqQGf6uiXiy8dzi"></div>
            </div>
            <input type="hidden" name="command" value="newpw">

            <button type="submit">Submit</button>
            <span>or use your account</span>

        </form>
    </div>
    <div class="overlay-container">
        <div class="overlay">
            <div class="overlay-panel overlay-left">
                <h1 class="title">Hello <br> friends</h1>
                <p>if Yout have an account, login here and have fun</p>
                <button class="ghost" id="login">Login
                    <i class="lni lni-arrow-left login"></i>
                </button>
            </div>
            <div class="overlay-panel overlay-right">
                <h1 class="title">Start yout <br> journy now</h1>
                <p>if you don't have an account yet, join us and start your journey.</p>

            </div>
        </div>
    </div>
</div>
</body>
</html>
