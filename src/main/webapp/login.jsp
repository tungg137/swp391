<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 1/20/2023
  Time: 4:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <%--css--%>

    <link rel="stylesheet" href="templateAdmin/css/index.css">
    <link rel="stylesheet" href="templateAdmin/css/captcha.css">
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>

</head>
<body>
<div class="container" id="container">
    <div class="form-container register-container">
        <form action="DangNhap" method="post">
            <h1>Register hire.</h1>

            <input name="username" type="text" placeholder="Name" required autofocus pattern="^[a-z\d\.]{6,}$"
                   title="Ít nhất 8 kí tự & không khoảng trắng">
            <input name="password" type="password" placeholder="Password" required
                   pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                   title="Ít nhất 8 kí tự bao gồm chữ hoa, chữ thường, số">
            <div style="color: red">
                <c:if test="${error1 != null}">
                    <div class="row">
                        <c:out value="${error1}"/>
                    </div>
                </c:if>
                <c:if test="${error2 != null}">
                    <div class="row">
                        <c:out value="${error2}"/>
                    </div>
                </c:if>
            </div>
            <input name="email" type="email" placeholder="Email">
            <!--captcha-->
            <div class="captcha_parent">
                <input class="name1" name="getCaptcha1" type="hidden"  placeholder="test"  >

                <div class="captcha-area">
                    <div class="captcha-img" style="">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQoAAAC+CAMAAAD6ObEsAAAAA1BMVEUApd9iZrozAAAASElEQVR4nO3BMQEAAADCoPVPbQ0PoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADeDcYqAAE00FRDAAAAAElFTkSuQmCC"
                             alt="Captch Background">
                        <span class="captcha1"></span>
                    </div>

                </div>

                <div class="input-area" >
                    <input type="text" name="enterCaptcha1" placeholder="Enter captcha" maxlength="6" spellcheck="false" required>
                </div>
            </div>
            <input  name="command" type="hidden" value="register"  >

            <button type="submit" onreset="this">Register</button>
        </form>
    </div>
    <div class="form-container login-container">
        <form action="DangNhap" method="post">
            <h1>Login here</h1>
            <input name="username" type="text" placeholder="username">
            <input name="password" type="password" placeholder="Password">
            <div style="color: red">
                <c:if test="${error1 != null}">
                    <c:out value="${error1}"/>
                </c:if>
                <c:if test="${error2 != null}">
                    <c:out value="${error2}"/>
                </c:if>
            </div>
            <!--captcha-->
            <div class="captcha_parent">
                <input class="name2"  name="getCaptcha2" type="hidden"  placeholder="test"  >

                <div class="captcha-area">
                    <div class="captcha-img" style="">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQoAAAC+CAMAAAD6ObEsAAAAA1BMVEUApd9iZrozAAAASElEQVR4nO3BMQEAAADCoPVPbQ0PoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADeDcYqAAE00FRDAAAAAElFTkSuQmCC"
                             alt="Captch Background">
                        <span class="captcha2"></span>
                    </div>

                </div>

                <div class="input-area" >
                    <input type="text" name="enterCaptcha2" placeholder="Enter captcha" maxlength="6" spellcheck="false" required>
                </div>
            </div>
            <div class="content">

                <div class="pass-link">
                    <a href="forgot.jsp">Forgot password?</a>
                </div>
            </div>
            <input type="hidden" name="command" value="login">
            <button type="submit">Login</button>
            <span>SWP391-GROUP 7</span>

        </form>
    </div>
    <div class="overlay-container">
        <div class="overlay">
            <div class="overlay-panel overlay-left">
                <h1 class="title">Hello <br> friends</h1>
                <p>if Yout have an account, login here and have fun</p>
                <button class="ghost" id="login">Login
                    <i class="lni lni-arrow-left login"></i>
                </button>
            </div>
            <div class="overlay-panel overlay-right">
                <h1 class="title">Start yout <br> journy now</h1>
                <p>if you don't have an account yet, join us and start your journey.</p>
                <button class="ghost" id="register">Register
                    <i class="lni lni-arrow-right register"></i>
                </button>
            </div>
        </div>
    </div>
</div>
</div>

<script src="templateAdmin/js/script.js"></script>
<script src="templateAdmin/js/captcha.js"></script>

</body>
</html>
