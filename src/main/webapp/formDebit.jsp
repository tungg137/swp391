<%@ page import="model.Debit" %>
<%@ page import="tool.Helper" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="dao.DaoForDebit" %><%--
  Created by IntelliJ IDEA.
  User: Non cai hand
  Date: 2/12/2023
  Time: 4:08 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="dao.DaoForDebit" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Account" %>
<%@ page import="dao.Dao" %>

<%
    DaoForDebit db = new DaoForDebit();
    List<Debit> list = db.listPeople();
    Account account = new Account();
%>

<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="templateAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="templateAdmin/css/formDebit.css">
    <title>SB Admin 2 - Tables</title>

    <!-- Custom fonts for this template -->
    <link href="templateAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="templateAdmin/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="templateAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!--Datetime picker-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">


</head>

<body id="page-top">
<input id="hehe" type="hidden" name="idGetDetailToLoad" value="1"/>
<!--click hien chi tiet 1 id_debit-->
<div class="chitiethienform">

</div>
<!--click de cong tru khoan no chi tiet-->
<div class="congtrukhoanno">

</div>
<!-- dialog -->
<div id="dialog-form" style="display: none" class="modal-content" title="Thông tin người nợ">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">


                <div class="card-body">
                    <div autocomplete="off">
                        <form action="Debit">
                            <input type="hidden" name="command" value="add"/>

                            <div class="position-relative row form-group">
                                <div class="form-label-horizontal col-md-3"><label class=""><b>Họ và tên (*)</b></label>
                                </div>
                                <div class="col-md-9"><input name="name" placeholder="" type="text" class="form-control"
                                                             value="">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <div class="form-label-horizontal col-md-3"><label class=""><b>Địa chỉ </b></label>
                                </div>
                                <div class="col-md-9"><textarea name="address" rows="8" placeholder=""
                                                                class="form-control"
                                                                style="height: 175px;"></textarea></div>
                            </div>
                            <div class="position-relative row form-group">
                                <div class="form-label-horizontal col-md-3"><label class=""><b>Sđt </b></label></div>
                                <div class="col-md-9"><input name="phone" placeholder="" type="text"
                                                             class="form-control" value="">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <div class="form-label-horizontal col-md-3"><label class=""><b>Email </b></label></div>
                                <div class="col-md-9"><input name="email" placeholder="" type="text"
                                                             class="form-control" value="">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <div class="form-label-horizontal col-md-3"><label class=""><b>Tổng nợ </b></label>
                                </div>
                                <div class="col-md-9"><input disabled="" placeholder="Input number" type="text"
                                                             class="form-control"
                                                             value="0">
                                    <div class="numberToWords">khong</div>
                                </div>
                            </div>
                            <div style="display: flex; justify-content: center; align-items: center;">
                                <button type="submit" class="mr-1 btn btn-success"><i class="fa fa-plus"></i> Thêm mới
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<!-- xem chi tiết -->
<%
    for (int i = 0; i < list.size(); i++) {

%>
<div style="display:none" class="card shadow mb-4 dialog-see-detail<%=list.get(i).getId()%>"
     title="<%=list.get(i).getName()%>">
    <div class="card-header">
        <div class="fa-pull-left">
            <h3 class="mb-0">Danh sách người nợ</h3>
            <p class="small text-muted m-0">Total: 10 Record(s)</p>
        </div>
        <div class="fa-pull-right">
            <button id="" type="button" data-toggle="tooltip" title="Thêm khoản nợ"
                    class="mr-1 btn btn-success id_no<%=list.get(i).getId()%>"><i class="fa fa-plus"></i> <span
                    class="thunho"> Thêm khoản nợ</span>
            </button>
        </div>
    </div>
    <div class="card-body bang<%=list.get(i).getId()%>">

    </div>
</div>
<% } %>
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="home.jsp">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            <div class="sidebar-brand-text mx-3">SWP391 <sup>2</sup></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="home.jsp">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Trang chủ</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Quản lý quỹ
        </div>
        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="Debit">
                <input type="hidden" name="command" value="load"/>

                <i class="fas fa-fw fa-table"></i>
                <span>Sổ ghi nợ</span></a>
        </li>


        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <form class="form-inline">
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                </form>

                <!-- Topbar Search -->
                <form
                        class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                               aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-search fa-fw"></i>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                             aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small"
                                           placeholder="Search for..." aria-label="Search"
                                           aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>

                    <!-- Nav Item - Alerts -->
                    <li class="nav-item dropdown no-arrow mx-1">
                        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-bell fa-fw"></i>
                            <!-- Counter - Alerts -->
                            <span class="badge badge-danger badge-counter">3+</span>
                        </a>
                        <!-- Dropdown - Alerts -->
                        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="alertsDropdown">
                            <h6 class="dropdown-header">
                                Alerts Center
                            </h6>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="mr-3">
                                    <div class="icon-circle bg-primary">
                                        <i class="fas fa-file-alt text-white"></i>
                                    </div>
                                </div>
                                <div>
                                    <div class="small text-gray-500">December 12, 2019</div>
                                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                                </div>
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="mr-3">
                                    <div class="icon-circle bg-success">
                                        <i class="fas fa-donate text-white"></i>
                                    </div>
                                </div>
                                <div>
                                    <div class="small text-gray-500">December 7, 2019</div>
                                    $290.29 has been deposited into your account!
                                </div>
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="mr-3">
                                    <div class="icon-circle bg-warning">
                                        <i class="fas fa-exclamation-triangle text-white"></i>
                                    </div>
                                </div>
                                <div>
                                    <div class="small text-gray-500">December 2, 2019</div>
                                    Spending Alert: We've noticed unusually high spending for your account.
                                </div>
                            </a>
                            <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                        </div>
                    </li>

                    <!-- Nav Item - Messages -->
                    <li class="nav-item dropdown no-arrow mx-1">
                        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-envelope fa-fw"></i>
                            <!-- Counter - Messages -->
                            <span class="badge badge-danger badge-counter">7</span>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="messagesDropdown">
                            <h6 class="dropdown-header">
                                Message Center
                            </h6>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="dropdown-list-image mr-3">
                                    <img class="rounded-circle" src="templateAdmin/img/undraw_profile_1.svg"
                                         alt="...">
                                    <div class="status-indicator bg-success"></div>
                                </div>
                                <div class="font-weight-bold">
                                    <div class="text-truncate">Hi there! I am wondering if you can help me with a
                                        problem I've been having.
                                    </div>
                                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                                </div>
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="dropdown-list-image mr-3">
                                    <img class="rounded-circle" src="templateAdmin/img/undraw_profile_2.svg"
                                         alt="...">
                                    <div class="status-indicator"></div>
                                </div>
                                <div>
                                    <div class="text-truncate">I have the photos that you ordered last month, how
                                        would you like them sent to you?
                                    </div>
                                    <div class="small text-gray-500">Jae Chun · 1d</div>
                                </div>
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="dropdown-list-image mr-3">
                                    <img class="rounded-circle" src="templateAdmin/img/undraw_profile_3.svg"
                                         alt="...">
                                    <div class="status-indicator bg-warning"></div>
                                </div>
                                <div>
                                    <div class="text-truncate">Last month's report looks great, I am very happy with
                                        the progress so far, keep up the good work!
                                    </div>
                                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                                </div>
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="dropdown-list-image mr-3">
                                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60"
                                         alt="...">
                                    <div class="status-indicator bg-success"></div>
                                </div>
                                <div>
                                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone
                                        told me that people say this to all dogs, even if they aren't good...
                                    </div>
                                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                                </div>
                            </a>
                            <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
                        </div>
                    </li>

                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><%=session.getAttribute("username")%></span>
                            <img class="img-profile rounded-circle"
                                 src="templateAdmin/img/undraw_profile.svg">
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                Profile
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Settings
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                Activity Log
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Tables</h1>
                <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
                    For more information about DataTables, please visit the <a target="_blank"
                                                                               href="https://datatables.net">official
                        DataTables documentation</a>.</p>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header">
                        <div class="fa-pull-left">
                            <h3 class="mb-0">Danh sách phiếu nợ</h3>
                            <p class="small text-muted m-0">Total: 10 Record(s)</p>
                        </div>
                        <div class="fa-pull-right">
                            <button id="dialog-open" type="button" data-toggle="tooltip" title="Thêm người nợ"
                                    class="mr-1 btn btn-success"><i class="fa fa-plus "></i><span class="thunho">Thêm người nợ</span>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <tr style="text-align: center;">
                                    <th>Id</th>
                                    <th>Tên</th>
                                    <th>Địa chỉ</th>
                                    <th>SĐT</th>
                                    <th>Email</th>
                                    <th>Tổng nợ</th>
                                    <th>Ngày tạo</th>
                                    <th>Cập nhật</th>
                                    <th>Action</th>
                                </tr>
                                <div style="text-align: center">
                                    <th>
                                        <label>
                                            <input id="column1Search" type="text" class="form-control"
                                                   placeholder="From"/>
                                        </label>
                                        <label>
                                            <input id="column1.1Search" type="text" class="form-control"
                                                   placeholder="To"/>
                                        </label>
                                    </th>
                                    <th><input id="column2Search" type="text" class="form-control"/></th>
                                    <th><input id="column3Search" type="text" class="form-control"/></th>
                                    <th><input id="column4Search" type="text" class="form-control"/></th>
                                    <th><input id="column5Search" type="text" class="form-control"/></th>
                                    <th>
                                        <label>
                                            <input id="column6Search" type="text" class="form-control"
                                                   placeholder="From"/>
                                        </label>
                                        <label>
                                            <input id="column6.1Search" type="text" class="form-control"
                                                   placeholder="To"/>
                                        </label>
                                    </th>
                                    <th>
                                        <label>
                                            <input type="datetime-local" placeholder="From" class="form-control"
                                                   value="">
                                        </label>
                                        <label>
                                            <input type="datetime-local" placeholder="To" class="form-control" value="">
                                        </label>
                                    </th>
                                    <th>
                                        <label>
                                            <input type="datetime-local" class="form-control" placeholder="From"/>
                                        </label>
                                        <label>
                                            <input type="datetime-local" class="form-control" placeholder="To"/>
                                        </label>
                                    </th>
                                    <th class="last_td">

                                        <div>
                                            <button type="button" class="btn btn-outline-danger btn-block action_big">
                                                CLEAR
                                                FILTER
                                            </button>
                                        </div>
                                        <div>
                                            <button type="button"
                                                    class="btn btn-outline-primary btn-block action_small"> HIDE >>
                                            </button>
                                        </div>
                                    </th>
                                </div>
                                <%--                                </tfoot>--%>
                                <%--                                <tbody>--%>
                                <%
                                    for (int i = 0; i < list.size(); i++) {
                                %>                                    <!-- sửa nợ -->
                                <div style="display: none" class="modal-content id<%=list.get(i).getId()%>"
                                     title="Thông tin người nợ">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">


                                                <div class="card-body">
                                                    <div autocomplete="off">
                                                        <form action="Debit">
                                                            <input type="hidden" name="command" value="update"/>
                                                            <input type="hidden" name="id"
                                                                   value="<%=list.get(i).getId()%>">
                                                            <div class="position-relative row form-group">
                                                                <div class="form-label-horizontal col-md-3"><label
                                                                        class=""><b>Họ và tên (*)</b></label>
                                                                </div>
                                                                <div class="col-md-9"><input name="name"
                                                                                             placeholder=""
                                                                                             type="text"
                                                                                             class="form-control"
                                                                                             value="<%=list.get(i).getName()%>">
                                                                </div>
                                                            </div>
                                                            <div class="position-relative row form-group">
                                                                <div class="form-label-horizontal col-md-3"><label
                                                                        class=""><b>Địa chỉ </b></label>
                                                                </div>
                                                                <div class="col-md-9"><textarea name="address"
                                                                                                rows="8"
                                                                                                placeholder=""
                                                                                                class="form-control"
                                                                                                style="height: 175px;"><%=list.get(i).getAddress()%></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="position-relative row form-group">
                                                                <div class="form-label-horizontal col-md-3"><label
                                                                        class=""><b>Sđt </b></label></div>
                                                                <div class="col-md-9"><input name="phone"
                                                                                             placeholder=""
                                                                                             type="text"
                                                                                             class="form-control"
                                                                                             value="<%=list.get(i).getPhone()%>">
                                                                </div>
                                                            </div>
                                                            <div class="position-relative row form-group">
                                                                <div class="form-label-horizontal col-md-3"><label
                                                                        class=""><b>Email </b></label></div>
                                                                <div class="col-md-9"><input name="email"
                                                                                             placeholder=""
                                                                                             type="text"
                                                                                             class="form-control"
                                                                                             value="<%=list.get(i).getEmail()%>">
                                                                </div>
                                                            </div>
                                                            <div class="position-relative row form-group">
                                                                <div class="form-label-horizontal col-md-3"><label
                                                                        class=""><b>Tổng nợ </b></label>
                                                                </div>
                                                                <div class="col-md-9"><input disabled=""
                                                                                             placeholder="Input number"
                                                                                             type="text"
                                                                                             class="form-control"
                                                                                             value="<%=list.get(i).getTotal()%>">
                                                                    <div class="numberToWords">không</div>
                                                                </div>
                                                            </div>
                                                            <div style="display: flex; justify-content: center; align-items: center;">
                                                                <button type="submit" class="mr-1 btn btn-success">
                                                                    <i class="fa fa-plus"></i> Cập nhật
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--Thêm phiếu nợ của 1 người-->
                                <div style="display: none" id="dialog-add-debit"
                                     class="modal-content id_noform<%=list.get(i).getId()%>" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">


                                                    <div class="card-body">
                                                        <div autocomplete="off">
                                                            <form action="Debit">
                                                                <input type="hidden" name="command"
                                                                       value="add_debit"/>
                                                                <input type="hidden" name="id"
                                                                       value="<%=list.get(i).getName()%>">
                                                                <div class="position-relative row form-group">
                                                                    <div class="form-label-horizontal col-md-3">
                                                                        <label class=""><b>Ghi chú </b></label>
                                                                    </div>
                                                                    <div class="col-md-9"><textarea name="note"
                                                                                                    rows="8"
                                                                                                    placeholder=""
                                                                                                    class="form-control"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="position-relative row form-group">
                                                                    <div class="form-label-horizontal col-md-3">
                                                                        <label class=""><b>Loại nợ
                                                                            (*)</b></label></div>
                                                                    <div class="col-md-9">
                                                                        <input name="" type="text"
                                                                               class="btn btn-secondary button1"
                                                                               value="-" readonly
                                                                               style="width: 49%; margin-right: 5px; margin-bottom: 5px;">

                                                                        <input name="cateria" type="text"
                                                                               class="btn btn-primary button2"
                                                                               value="+" readonly
                                                                               style="width: 49%; margin-right: 5px; margin-bottom: 5px;">

                                                                    </div>
                                                                </div>
                                                                <div class="position-relative row form-group">
                                                                    <div class="form-label-horizontal col-md-3">
                                                                        <label class=""><b>Số tiền
                                                                            (*)</b></label></div>
                                                                    <div class="col-md-9"><input name="money"
                                                                                                 placeholder="Input number"
                                                                                                 type="text"
                                                                                                 class="form-control cnb"
                                                                                                 onkeypress="return onlyNumbers(this.value);"
                                                                                                 onkeyup="NumToWord(this.value,'divDisplayWords<%=list.get(i).getId()%>');"/>
                                                                        <div id="divDisplayWords<%=list.get(i).getId()%>"
                                                                             style="color: Teal; font-family: Arial;">
                                                                            Không
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="position-relative row form-group">
                                                                    <div class="form-label-horizontal col-md-3">
                                                                        <label class=""><b>Ngày lập
                                                                            phiếu </b></label></div>
                                                                    <div class="col-md-9">
                                                                        <div>
                                                                            <div class="react-datepicker-wrapper">
                                                                                <div class="react-datepicker__input-container">
                                                                                    <input name="crtDtTs"
                                                                                           type="datetime-local"
                                                                                           placeholder="DD/MM/YYYY HH:mm"
                                                                                           class="form-control full-width"
                                                                                           value=""></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="position-relative row form-group">
                                                                    <div class="form-label-horizontal col-md-3">
                                                                        <label class=""><b>Hình ảnh chứng
                                                                            minh </b></label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <div>
                                                                            <input type="file" name="image"
                                                                                   class="btn btn-primary">


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="display: flex; justify-content: center; align-items: center;">
                                                                    <button type="submit"
                                                                            class="mr-1 btn btn-success"><i
                                                                            class="fa fa-plus"></i> Thêm
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <tr style="text-align: center;">
                                    <td><%=Helper.format(list.get(i).getId())%>
                                    </td>
                                    <td><%=Helper.cutString(list.get(i).getName(), 5)%>
                                    </td>
                                    <td><%=Helper.cutString(list.get(i).getAddress(), 5)%>
                                    </td>
                                    <td><%=list.get(i).getPhone()%>
                                    </td>
                                    <td><%=Helper.cutString(list.get(i).getEmail(), 5)%>
                                    </td>
                                    <td><%=Helper.format(list.get(i).getTotal())%>
                                    </td>
                                    <td><%=list.get(i).getCrtDtTs()%>
                                    </td>
                                    <td><%=list.get(i).getUpDtTs()%>
                                    </td>


                                    <td style="display: flex;flex-direction: row;">
                                        <div>
                                            <button id="detail" type="button"
                                                    class="mr-1 btn btn-info detail<%=list.get(i).getId()%>"><i
                                                    class="fa fa-eye eye<%=list.get(i).getId()%>"
                                                    title="Chi tiết"></i><span class="thunho">Chi tiết</span>
                                            </button>
                                        </div>
                                        <div>
                                            <button type="button" id="them_no"
                                                    class="mr-1 btn btn-outline-success id_no<%=list.get(i).getId()%>">
                                                <i
                                                        class="fa fa-plus plus<%=list.get(i).getId()%>"
                                                        title="Thêm phiếu nợ"></i><span class="thunho">Thêm nợ</span>
                                            </button>
                                        </div>
                                        <div>
                                            <button type="button"
                                                    class="btn btn-outline-warning id_person<%=list.get(i).getId()%>"><i
                                                    class="fa fa-edit edit<%=list.get(i).getId()%>"
                                                    title="Sửa"></i><span class="thunho">Sửa nợ</span>
                                            </button>
                                        </div>
                                    </td>

                                </tr>

                                <% } %>
                                <%--                                </tbody>--%>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Your Website 2020</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="logout" method="post">
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Logout</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.js"></script>

<!-- Bootstrap core JavaScript-->
<script src="templateAdmin/vendor/jquery/jquery.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="templateAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="templateAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
<!-- Page level plugins -->
<script src="templateAdmin/vendor/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/jquery.dataTables.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="templateAdmin/js/sb-admin-2.min.js"></script>
<script src="templateAdmin/js/dialog.js"></script>

<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script src="templateAdmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!--Date time picker-->
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    config = {
        enableTime: true,
        dateFormat: "Y-m-d H:i",
    }
    flatpickr("input[type=datetime-local]", config);
</script>

<script src="templateAdmin/js/numberToWords.js">

</script>
<script src="templateAdmin/js/demo/collapse.js"></script>
<script src="templateAdmin/js/demo/search.js"></script>

</body>
</html>
