<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 1/21/2023
  Time: 12:18 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <%--css--%>
    <link rel="stylesheet" href="templateAdmin/css/index.css">
    <script src="https://www.google.com/recaptcha/api.js"></script>


</head>
<body>
<div class="container" id="container">

<div class="form-container register-container">
    <form action="DangNhap" method="post">
        <h1>Forgot password.</h1>
        <input name="username" type="text" placeholder="Name">
        <div >
            <c:if test="${error != null}">
                <div class="row" style="background-color: red; padding: 20px 60px">
                    <c:out value="${error}"/>
                </div>
            </c:if>
        </div>

        <input name="email" type="email" placeholder="Email">
        <!--captcha-->
        <div class="row">
            <div class="g-recaptcha"
                 data-sitekey="6Lfp3gkkAAAAAA9sGm4QNzkc0pqQGf6uiXiy8dzi"></div>
        </div>
        <input type="hidden" name="command" value="forgot">

        <button type="submit">Submit</button>
        <span>or use your account</span>

    </form>
</div>
<div class="overlay-container">
    <div class="overlay">
        <div class="overlay-panel overlay-left">
            <h1 class="title">Hello <br> friends</h1>
            <p>if Yout have an account, login here and have fun</p>
            <button class="ghost" id="login">Login
                <i class="lni lni-arrow-left login"></i>
            </button>
        </div>
        <div class="overlay-panel overlay-right">
            <h1 class="title">Start yout <br> journy now</h1>
            <p>if you don't have an account yet, join us and start your journey.</p>

        </div>
    </div>
</div>
</div>
</body>
</html>
