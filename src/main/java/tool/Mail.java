package tool;

import model.Account;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.util.Date;
import java.util.Properties;


public class Mail {
    public static String message(Account user, String code) {
        String linkActive = "http://credit.fpt.vn:8002/SWP391/active?username=" + user.getUsername() + "&code=" + code;
        String str = "<html lang=\"en\">\n"
                + "<head>\n"
                + "  <meta charset=\"UTF-8\">\n"
                + "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "  <title>Document</title>\n"
                + "</head>\n"
                + "<body>\n"
                + "  <h2>Chào mừng:" + user.getUsername() + "</h2>\n"
                + "  <p>Chúng tôi đã nhận được yêu cầu mở tài khoản cho FPTCREDIT với thông tin chính như sau</p>\n"
                + "  <p>Tài khoản: " + user.getUsername() + "</p>\n"
                + "  <p>Email: " + user.getEmail() + "</p>\n"
                + "  <p>Mật khẩu:" + user.getPassword() + "</p>\n"
                + "<p>Vui lòng nhập link sau để xác thực:" + linkActive + "</p>\n"
                + "</body>\n"
                + "</html>";
        return str;
    }

    public static String messageForgotPs(Account user, String code) {
        String linkActive = "http://credit.fpt.vn:8002/SWP391/forgot?username=" + user.getUsername() + "&code=" + code;
        String str = "<html lang=\"en\">\n"
                + "<head>\n"
                + "  <meta charset=\"UTF-8\">\n"
                + "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "  <title>Document</title>\n"
                + "</head>\n"
                + "<body>\n"
                + "  <h2>Chào mừng:" + user.getUsername() + "</h2>\n"
                + "  <p>Chúng tôi đã nhận được yêu cầu lấy lại mâ khẩu với thông tin chính như sau</p>\n"
                + "  <p>Tài khoản: " + user.getUsername() + "</p>\n"
                + "  <p>Email: " + user.getEmail() + "</p>\n"
                + "<p>Vui lòng nhập link sau để xác thực:" + linkActive + "</p>\n"
                + "</body>\n"
                + "</html>";
        return str;
    }

    public static boolean sendMail(String to, String subject, String message) {
        try {
            String host = "smtp.gmail.com";
            String user = "vinhvinh307@gmail.com";
            String pass = "iopzgjsuqpoohkkc";
            String from = "vinhvinh307@gmail.com";
//            String messageText = "Your Is Test Email :";
            boolean sessionDebug = false;

            Properties props = System.getProperties();


            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.protocols", "TLSv1.2");
            props.put("mail.smtp.starttls.enable", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(MimeUtility.encodeText(subject, "utf-8", "B"));
            msg.setSentDate(new Date());
//          msg.setText(messageText);
            msg.setContent(message, "text/html; charset=utf-8");

            Transport transport = mailSession.getTransport("smtp");
            transport.connect(host, user, pass);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(sendMail("vinhtnhe163160@fpt.edu.vn", "active tai khoan", "23"));
    }
}
