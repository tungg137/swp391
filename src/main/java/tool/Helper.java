package tool;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;

/**
 *
 * @author vinhv
 */
public class Helper {
    public static String format(int number){
        NumberFormat numberformat = NumberFormat.getInstance(Locale.ITALY) ;
        return numberformat.format(number) ;
    }

    public static String cutString(String string,int size){
        if(string.length() > size){
            string = string.substring(0,size) ;
            string = string + "..." ;
        }
        return string ;
    }

    public static String randomKey(int length){
        String str = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ;
        Random rand = new Random() ;
        StringBuilder sb = new StringBuilder() ;
        for(int i = 0 ; i < length ;i++){
            sb.append(str.charAt(rand.nextInt(str.length()))) ;
        }
        return sb.toString() ;
    }

    public static void main(String[] args) {
        System.out.println(cutString("2WERTREWE34435",5));
        System.out.println(format(1000000000));

    }
}
