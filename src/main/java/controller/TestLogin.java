package controller;

import dao.Dao;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "TestLogin", value = "/TestLogin")
public class TestLogin extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        Dao dao = new Dao() ;
        HttpSession session = request.getSession();
        String user = request.getParameter("username");
        String pwd = request.getParameter("password");
        String getCaptcha = request.getParameter("getCaptcha2");
        String enterCaptcha = request.getParameter("enterCaptcha2");
        //dao.login(user,pwd) != null
        try {
            if(user.equals("") || pwd.equals("")||enterCaptcha.equals("")){
                out.write("Please fill out this field");
                return ;

            }
            if(getCaptcha.equals(enterCaptcha)  && Dao.login(user,pwd) != null ){
                if(Dao.login(user,pwd).getActive() == 1){
                    session.setAttribute("username",user);
                    out.write("Login successfull");

                    request.getRequestDispatcher("/login.jsp").forward(request,response);
                }
            }else{
                request.setAttribute("error2","Your account is wrong or miss captcha!");
                out.write("Invalid username, password or miss captcha");

                request.getRequestDispatcher("/login.jsp").forward(request,response);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
