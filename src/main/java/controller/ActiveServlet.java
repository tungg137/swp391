package controller;

import dao.Dao;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "ActiveServlet", value = "/active")
public class ActiveServlet extends HttpServlet {
    Dao dao = new Dao();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username") ;
        String code = request.getParameter("code");
        try {
            if(dao.activeUser(username,code)){
                request.setAttribute("error","Your account has actived!");
                request.getRequestDispatcher("/login.jsp").forward(request,response);
            }else{
                request.setAttribute("error","Active failed!");
                request.getRequestDispatcher("/login.jsp").forward(request,response);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
