package controller;

import dao.Dao;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "CheckUsername", value = "/checkun")
public class CheckUsername extends HttpServlet {
    Dao dao = new Dao() ;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter() ;
        try {
            if(dao.checkUsername(request.getParameter("username"))) {
                out.println("<img src=\""+request.getContextPath()+"/gif/delete.png\" > "
                        + "<input type=\"hidden\" id=\"user\" value=\"false\">");
            }else{
                out.println("<img src=\""+request.getContextPath()+"/gif/check.png\" > "
                        + "<input type=\"hidden\" id=\"user\" value=\"true\">");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
