package controller;

import dao.DaoForDebit;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import model.DebitDetail;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "Debit", value = "/Debit")
public class Debit extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter("command");
        try {
            if(command == null){
                command = "load" ;
            }

            switch (command) {
                case "load":
                    load(request, response);
                    load_detail(request,response);
                    break;
                case "add":
                    add(request, response);
                    break;
                case "update":
                    update(request,response);
                    break;
                case "add_debit":
                    addDebit(request,response);
                    break;
                default:
                    load(request, response);

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void load_detail(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("");
    }

    private void addDebit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id") );
        String note = request.getParameter("note");
        int cateria ;
        String cateria_inc = request.getParameter("cateria");
        if(cateria_inc.equals("+")) {
            cateria = 1 ;
        }else{
            cateria = 0 ;
        }

        float money = Float.parseFloat(request.getParameter("money"))  ;
        Object crtDtTs = request.getParameter("crtDtTs");
        Object image = request.getParameter("image");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        DebitDetail db = new DebitDetail(id,note,cateria,money,crtDtTs,dtf.format(now),image);
        DaoForDebit dao = new DaoForDebit() ;
        dao.insertDetail(db) ;
        load(request,response);

    }

    private void update(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        DaoForDebit db = new DaoForDebit() ;
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        int phone = Integer.parseInt(request.getParameter("phone"));
        String email = request.getParameter("email") ;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        model.Debit debit = new model.Debit(id,name,address, phone,email,dtf.format(now));
        db.updateInfo(debit) ;
        load(request,response);

    }



    private void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        int phone = Integer.parseInt(request.getParameter("phone")) ;
        String email = request.getParameter("email");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        model.Debit debit = new model.Debit(name,address,phone,email,dtf.format(now),dtf.format(now));
        DaoForDebit dao = new DaoForDebit() ;

        dao.insert(debit);
        load(request,response);
    }

    private void load(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DaoForDebit dao = new DaoForDebit() ;
        List<model.Debit> list = dao.listPeople() ;
        request.setAttribute("list",list);
        request.getRequestDispatcher("formDebit.jsp").forward(request,response);
    }


}
