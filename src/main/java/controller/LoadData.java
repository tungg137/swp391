package controller;

import dao.DaoForDebit;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import model.Debit;
import model.DebitDetail;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.DoubleToIntFunction;

@WebServlet(name = "loaddata", value = "/loaddata")
public class LoadData extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        switch (name) {
            case "loadperson":
                loadperson(request, response);
                break;
            case "loaddetail":
                loaddetail(request, response);
                break;
            case "congtruno":
                congtruno(request, response);
                break;
            case "addadebit":
                add(request,response);
                break ;
        }


    }

    private void add(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter("ids") );
        String note = request.getParameter("note");
        int cateria ;
        String cateria_inc = request.getParameter("cateria");
        if(cateria_inc.equals("+")) {
            cateria = 1 ;
        }else{
            cateria = 0 ;
        }

        float money = Float.parseFloat(request.getParameter("money"))  ;
        Object crtDtTs = request.getParameter("date");
        Object image = request.getParameter("image");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        DebitDetail db = new DebitDetail(id,note,cateria,money,crtDtTs,dtf.format(now),image);
        DaoForDebit dao = new DaoForDebit() ;

        PrintWriter out = response.getWriter();
        dao.insertDetail(db) ;



    }

    private void congtruno(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();

        String id_debit = request.getParameter("id_debit");
        DaoForDebit dao = new DaoForDebit();
        List<DebitDetail> list = dao.listdetail(Integer.parseInt(id_debit));
        out.println("<div class=\"modal-content\">" +
                "    <div class=\"modal-body\">" +
                "      <div class=\"row\">" +
                "        <div class=\"col-md-12\">" +
                "" +
                "" +
                "          <div class=\"card-body\">" +
                "            <div autocomplete=\"off\">" +

                "                <div class=\"position-relative row form-group\">" +
                "                  <div class=\"form-label-horizontal col-md-3\">" +
                "                    <label class=\"\"><b>Ghi chú </b></label>" +
                "                  </div>" +
                "                  <div class=\"col-md-9\"><textarea name=\"note\" rows=\"8\" placeholder=\"\" class=\"form-control noto\">Thanh toán cho phiếu nợ số " + list.get(0).getId_debit() + "</textarea>\n" +
                "                  </div></div>" +
                "<div class=\"position-relative row form-group\">" +
                "                  <div class=\"form-label-horizontal col-md-3\">" +
                "                    <label class=\"\"><b>Loại nợ" +
                "                        (*)</b></label>" +
                "                  </div>");
        if(list.get(0).getCateria() == 1) {
            out.println(
                    "                  <div class=\"col-md-9\">" +
                            "                    <input name=\"\" type=\"text\" class=\"btn btn-primary button1\" value=\"-\" readonly" +
                            "                      style=\"width: 49%; margin-right: 5px; margin-bottom: 5px;\">" +
                            "" +
                            "                    <input name=\"cateria\" type=\"text\" class=\"btn btn-secondary button2\" value=\"+\" readonly" +
                            "                      style=\"width: 49%; margin-right: 5px; margin-bottom: 5px;\">" +
                            "" +
                            "                  </div>");
        }else{
            out.println(
                    "                  <div class=\"col-md-9\">" +
                            "                    <input name=\"\" type=\"text\" class=\"btn btn-secondary button1\" value=\"-\" readonly" +
                            "                      style=\"width: 49%; margin-right: 5px; margin-bottom: 5px;\">" +
                            "" +
                            "                    <input name=\"cateria\" type=\"text\" class=\"btn btn-primary button2\" value=\"+\" readonly" +
                            "                      style=\"width: 49%; margin-right: 5px; margin-bottom: 5px;\">" +
                            "" +
                            "                  </div>");
        }
        out.println(
                "                </div>" +
                        "                <div class=\"position-relative row form-group\">" +
                        "                  <div class=\"form-label-horizontal col-md-3\">" +
                        "                    <label class=\"\"><b>Số tiền" +
                        "                        (*)</b></label>" +
                        "                  </div>" +
                        "                  <div class=\"col-md-9\"><input name=\"money\" placeholder=\"Input number\" type=\"text\" class=\"form-control money\"" +
                        "                      value=\"" + list.get(0).getMoney() + "\">" +
                        "                    <div class=\"\">không</div>" +
                        "                  </div>" +
                        "                </div>" +
                        "                <div class=\"position-relative row form-group\">" +
                        "                  <div class=\"form-label-horizontal col-md-3\">" +
                        "                    <label class=\"\"><b>Ngày lập phiếu" +
                        "                        </b></label>" +
                        "                  </div>" +
                        "                  <div class=\"col-md-9\">" +
                        "                    <div>" +
                        "                      <div class=\"react-datepicker-wrapper\">" +
                        "                        <div class=\"react-datepicker__input-container\">" +
                        "                          <input name=\"crtDtTs\" type=\"datetime-local\" placeholder=\"DD/MM/YYYY HH:mm\"" +
                        "                            class=\"form-control full-width date\" value=\""+list.get(0).getCrtDtTs()+"\">" +
                        "                        </div>" +
                        "                      </div>" +
                        "                    </div>" +
                        "                  </div>" +
                        "                </div>" +
                        "                <div class=\"position-relative row form-group\">" +
                        "                  <div class=\"form-label-horizontal col-md-3\">" +
                        "                    <label class=\"\"><b>Hình ảnh chứng minh" +
                        "                        minh </b></label>" +
                        "                  </div>" +
                        "                  <div class=\"col-md-9\">" +
                        "                    <div>" +
                        "                      <input type=\"file\" name=\"image\" class=\"btn btn-primary image\">" +
                        "" +
                        "" +
                        "                    </div>" +
                        "                  </div>" +
                        "                </div>" +
                        "                <div style=\"display: flex; justify-content: center; align-items: center;\">" +
                        "                  <input type=\"submit\" class=\"add\"><i class=\"fa fa-plus\"></i> Thêm" +
                        "                  </input>" +
                        "                </div>" +

                        "            </div>" +
                        "          </div>" +
                        "" +
                        "" +
                        "        </div>" +
                        "      </div>" +
                        "    </div>" +
                        "  </div>");


    }

    private void loaddetail(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();

        String id_debit = request.getParameter("id_debit");
        DaoForDebit dao = new DaoForDebit();
        List<DebitDetail> list = dao.listdetail(Integer.parseInt(id_debit));
        out.println("<div class=\"modal-content\" >" +
                "    <div class=\"modal-body\">" +
                "      <div class=\"row\">" +
                "        <div class=\"col-md-12\">" +
                "          <div class=\"card-group\">" +
                "            <div class=\"card\">" +

                "              <div class=\"card-body\">" +
                "                <div autocomplete=\"off\">" +
                "                  <div class=\"position-relative row form-group\">" +
                "                    <div class=\"form-label-horizontal col-md-3\"><label class=\"\"><b>Ghi chú </b></label></div>" +
                "                    <div class=\"col-md-9\"><textarea rows=\"8\" placeholder=\"\" disabled=\"\"" +
                "                        class=\"form-control\">" + list.get(0).getNote() + "</textarea></div></div>" +
                "<div class=\"position-relative row form-group\">" +
                "                    <div class=\"form-label-horizontal col-md-3\"><label class=\"\"><b>Loại nợ (*)</b></label></div>");

        if (list.get(0).getCateria() == 1) {
            out.println("<div class=\"col-md-9\"><button type=\"button\" disabled=\"\" class=\"btn btn-secondary disabled\"" +
                    "                        style=\"width: 49%; margin-right: 5px; margin-bottom: 5px;\">-</button><button type=\"button\"" +
                    "                        disabled=\"\" class=\"btn btn-primary disabled\"" +
                    "                        style=\"width: 49%; margin-right: 5px; margin-bottom: 5px;\">+</button></div>");
        } else {
            out.println("<div class=\"col-md-9\"><button type=\"button\" disabled=\"\" class=\"btn btn-primary disabled\"" +
                    "                        style=\"width: 49%; margin-right: 5px; margin-bottom: 5px;\">-</button><button type=\"button\"" +
                    "                        disabled=\"\" class=\"btn btn-secondary disabled\"" +
                    "                        style=\"width: 49%; margin-right: 5px; margin-bottom: 5px;\">+</button></div>");
        }
        out.println(
                "                  </div>" +
                        "                  <div class=\"position-relative row form-group\">" +
                        "                    <div class=\"form-label-horizontal col-md-3\"><label class=\"\"><b>Số tiền (*)</b></label></div>" +
                        "                    <div class=\"col-md-9\"><input disabled=\"\" placeholder=\"Input number\" type=\"text\" class=\"form-control\"" +
                        "                        value=\"" + list.get(0).getMoney() + "\">" +
                        "                      <div class=\"\">Mười hai nghìn</div>" +
                        "                    </div>" +
                        "                  </div>" +
                        "                  <div class=\"position-relative row form-group\">" +
                        "                    <div class=\"form-label-horizontal col-md-3\"><label class=\"\"><b>Ngày lập phiếu</b></label></div>" +
                        "                    <div class=\"col-md-9\">" +
                        "                      <div>" +
                        "                        <div class=\"react-datepicker-wrapper\">" +
                        "                          <div class=\"react-datepicker__input-container\"><input type=\"text\"" +
                        "                              placeholder=\"DD/MM/YYYY HH:mm\" disabled=\"\" class=\"form-control full-width\"" +
                        "                              value=\"" + list.get(0).getCrtDtTs() + "\"></div>" +
                        "                        </div>" +
                        "                      </div>" +
                        "                    </div>" +
                        "                  </div>" +
                        "                  <div class=\"position-relative row form-group\">" +
                        "                    <div class=\"form-label-horizontal col-md-3\"><label class=\"\"><b>Hình ảnh chứng minh </b></label>" +
                        "                    </div>" +
                        "                    <div class=\"col-md-9\">" +
                        "                      <div><button type=\"button\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i> Thêm ảnh</button>" +
                        "                      </div>" +
                        "                    </div>" +
                        "                  </div>" +
                        "                  <div style=\"display: flex; justify-content: center; align-items: center;\"></div>" +
                        "                </div>" +
                        "              </div>" +
                        "            </div>" +
                        "          </div>" +
                        "        </div>" +
                        "      </div>" +
                        "    </div>" +

                        "  </div>");


    }

    private void loadperson(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        DaoForDebit dao = new DaoForDebit();
        String id = request.getParameter("id");
        List<DebitDetail> list = dao.loadDetail(Integer.parseInt(id));
        // request.setAttribute("list",list);


        out.println("<div class=\"table-responsive\">" +
                "            <table  class=\"table table-bordered detail\" id=\"dataTable\" width=\"100%\"" +
                "                    cellspacing=\"0\">" +
                "                <thead>" +
                "                <tr style=\"text-align: center;\">" +
                "                    <th>Id</th>" +
                "                    <th>Ghi chú</th>" +
                "                    <th>Loại nợ</th>" +
                "                    <th>Số tiền</th>" +
                "                    <th>Ngày lập phiếu</th>" +
                "                    <th>Thời Gian tạo</th>" +
                "                    <th>Action</th>" +
                "                </tr>" +
                "" +
                "                </thead>" +
                "                <tbody>" +
                "" +
                "                <tr style=\"text-align: center;\">" +
                "                    <td>" +
                "                        <label>" +
                "                            <input type=\"text\" class=\"form-control\"" +
                "                                   placeholder=\"From\"/>" +
                "                        </label>" +
                "                        <label>" +
                "                            <input type=\"text\" class=\"form-control\"" +
                "                                   placeholder=\"To\"/>" +
                "                        </label>" +
                "                    </td>" +
                "                    <td><input type=\"text\" class=\"form-control\"/></td>" +
                "                    <td><input type=\"text\" class=\"form-control\"/></td>" +
                "                    <td>" +
                "                        <label>" +
                "                            <input type=\"text\" class=\"form-control\"" +
                "                                   placeholder=\"From\"/>" +
                "                        </label>" +
                "                        <label>" +
                "                            <input type=\"text\" class=\"form-control\"" +
                "                                   placeholder=\"To\"/>" +
                "                        </label>" +
                "                    </td>" +
                "" +
                "                    <td>" +
                "                        <label>" +
                "                            <input type=\"datetime-local\" placeholder=\"From\"" +
                "                                   class=\"form-control\" value=\"\">" +
                "                        </label>" +
                "                        <label>" +
                "                            <input type=\"datetime-local\" placeholder=\"To\"" +
                "                                   class=\"form-control\" value=\"\">" +
                "                        </label>" +
                "                    </td>" +
                "                    <td>" +
                "                        <label>" +
                "                            <input type=\"datetime-local\" placeholder=\"From\"" +
                "                                   class=\"form-control\" value=\"\">" +
                "                        </label>" +
                "                        <label>" +
                "                            <input type=\"datetime-local\" placeholder=\"To\"" +
                "                                   class=\"form-control\" value=\"\">" +
                "                        </label>" +
                "                    </td>" +
                "" +
                "                    <td  style=\"right: 0px; flex: 322 0 auto; width: 322px; max-width: 322px;\">" +
                "" +
                "                        <div>" +
                "                            <button type=\"button\"" +
                "                                    class=\"btn btn-outline-danger btn-block\">CLEAR" +
                "                                FILTER" +
                "                            </button>" +
                "                        </div>" +
                "                        <div>" +
                "                            <button type=\"button\"" +
                "                                    class=\"btn btn-outline-primary btn-block action_small\"> " +
                "                                HIDE >>" +
                "                            </button>" +
                "                        </div>" +
                "                    </td>" +
                "                </tr>");
        for (int i = 0; i < list.size(); i++) {
            out.println("<tr style=\"text-align: center;\">" +
                    "                    <td>" + list.get(i).getId_debit() + "</td>" +
                    "                    <td>" + list.get(i).getNote() + "</td>");
            if (list.get(i).getCateria() == 1) {
                out.println("                    <td>+" + "</td>");
            } else {
                out.println("                    <td>-" + "</td>");
            }

            out.println("                    <td>" + list.get(i).getMoney() + "</td>" +
                    "                    <td>" + list.get(i).getCrtDtTs() + "</td>" +
                    "                    <td>" + list.get(i).getUpDtTs() + "</td>" +
                    "" +
                    "" +
                    "                    <td style=\"display: flex;flex-direction: row;\">" +
                    "                        <div>" +
                    "                            <button id=\"detail\" type=\"button\"" +
                    "                                    class=\"mr-1 btn btn-info chitiet" + list.get(i).getId_debit() + "\"><i" +
                    "                                    class=\"fa fa-eye\"" +
                    "                                    title=\"Chi tiết\"></i><span class=\"thunho\">Chi tiết</span></i>" +
                    "                            </button>" +
                    "                        </div>");
            if (list.get(i).getCateria() == 1) {
                out.println(
                        "                        <div>" +
                                "                            <button type=\"button\" id=\"them_no\" style=\"opacity:0.3;\"" +
                                "                                    class=\"mr-1 btn btn-outline-success congno disbled" + list.get(i).getId_debit() + "\">" +
                                "                                <i" +
                                "                                        class=\"fa fa-plus\" title=\"Thêm phiếu nợ\"></i><span class=\"thunho\">Cộng nợ</span>" +
                                "                            </button>" +
                                "                        </div>" +
                                "                        <div>" +
                                "                            <button type=\"button\"" +
                                "                                    class=\"btn btn-outline-warning congtru" + list.get(i).getId_debit() + "\"><i" +
                                "                                    class=\"fa fa-minus\"" +
                                "                                    title=\"Trừ nợ\"></i><span class=\"thunho\">Trừ nợ</span>" +
                                "                            </button>" +
                                "                        </div>");
            } else {
                out.println(
                        "                        <div>" +
                                "                            <button type=\"button\" id=\"them_no\" " +
                                "                                    class=\"mr-1 btn btn-outline-success congtru" + list.get(i).getId_debit() + "\">" +
                                "                                <i" +
                                "                                        class=\"fa fa-plus\" title=\"Thêm phiếu nợ\"></i><span class=\"thunho\">Cộng nợ</span>" +
                                "                            </button>" +
                                "                        </div>" +
                                "                        <div>" +
                                "                            <button type=\"button\" style=\"opacity:0.3;\"" +
                                "                                    class=\"btn btn-outline-warning disbled congtru" + list.get(i).getId_debit() + "\"><i" +
                                "                                    class=\"fa fa-minus\"" +
                                "                                    title=\"Trừ nợ\"></i><span class=\"thunho\">Trừ nợ</span>" +
                                "                            </button>" +
                                "                        </div>");
            }

            out.println(
                    "                    </td>");

            out.println("</tr>");
        }
        out.println("<tbody></tbody>");
        out.println("<table></table>");
    }
}
