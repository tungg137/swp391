package controller;

import dao.Dao;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import model.VerifyRecaptcha;
import model.Account;

import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "DangNhap", value = "/DangNhap")
public class LoginController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public LoginController() {
        super();
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        String command = request.getParameter("command") ;
        switch (command){
            case "register":
                register(request,response);
                break;
            case "login"   :

                try {
                    login(request,response);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }

                break;
            case "forgot":
                try {
                    forgot(request,response);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                break;
            case "newpw":
                try {
                    newpw(request,response);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                break;
            default:
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/login.jsp").forward(req,resp);
    }

    private void newpw(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ServletException {
        Dao dao = new Dao() ;
        String password = request.getParameter("password");
        String username = request.getParameter("username");
        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
        boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
        if(dao.changePassword(username,password) && verify) {
            request.setAttribute("error","Thay đổi mật khẩu thành công!");
        }else{
            request.setAttribute("error","Thay đổi thất bại do bạn chưa click captcha");
            request.getRequestDispatcher("/login.jsp").forward(request,response);
        }





    }

    private void forgot(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        Dao dao = new Dao() ;
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
        boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
        if(dao.checkforgot(username,email) != null && verify){
            dao.sendEmtoForgot(dao.checkforgot(username,email)) ;
            request.setAttribute("error","yêu cầu đã được nhận, vui lòng check email để xác thực!");

        }else {
            request.setAttribute("error","Tài khoản, email sai hoặc chưa click captcha!");
            request.getRequestDispatcher("/forgot.jsp").forward(request,response);
        }
    }

    private void login(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        Dao dao = new Dao() ;
        HttpSession session = request.getSession();
        String user = request.getParameter("username");
        String pwd = request.getParameter("password");
        String getCaptcha = request.getParameter("getCaptcha2");
        String enterCaptcha = request.getParameter("enterCaptcha2");
        //dao.login(user,pwd) != null
        if(getCaptcha.equals(enterCaptcha)  && Dao.login(user,pwd) != null ){
            if(Dao.login(user,pwd).getActive() == 1){
                session.setAttribute("username",user);
                response.sendRedirect("home.jsp");
            }else{
                request.setAttribute("error2","please check email to active your account!");
            }
        }else{
            request.setAttribute("error2","Your account is wrong or miss captcha!");

            request.getRequestDispatcher("/login.jsp").forward(request,response);
        }

    }

    private void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Dao dao = new Dao() ;

        // get request parameters for userID and password
        String user = request.getParameter("username");
        String pwd = request.getParameter("password");
        String email = request.getParameter("email");
        String getCaptcha = request.getParameter("getCaptcha1");
        String enterCaptcha = request.getParameter("enterCaptcha1");
        Account ac = new Account(user,pwd,email) ;

//		System.out.println("User= " + user + " ::password= " + pwd + " ::Captcha Verify "+verify);

        if (getCaptcha.equals(enterCaptcha)) {
            Dao.insert(ac);
            request.setAttribute("error1","Succesfully, check email to active your account!");
        }else {
            request.setAttribute("error1","You miss the captcha.");
            request.getRequestDispatcher("/register.jsp").forward(request,response);

        }

    }


}
