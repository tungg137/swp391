package model;

public class Account {
    private String username ;
    private String password;
    private String email;
    private int active ;

    public Account(){}

    public Account(String username, String password, String email, int active) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.active = active;
    }

    public Account(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;

    } public Account(String username, String password) {
        this.username = username;
        this.password = password;
        this.email = email;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

}
