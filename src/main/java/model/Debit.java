package model;

public class Debit {
    private int id ;
    private String name ;
    private String address ;
    private int phone ;
    private String email ;
    private int total ;
    private Object crtDtTs ;
    private Object upDtTs ;

    public Debit(){};
    public Debit(int id, String name, String address, int phone, String email, int total, Object crtDtTs, Object upDtTs) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.total = total;
        this.crtDtTs = crtDtTs;
        this.upDtTs = upDtTs;
    }
    public Debit(String name, String address, int phone, String email, Object crtDtTs, Object upDtTs) {

        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.crtDtTs = crtDtTs;
        this.upDtTs = upDtTs;
    }

    public Debit(int id,String name, String address, int phone, String email, Object upDtTs) {
        this.id = id ;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.upDtTs = upDtTs ;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Object getCrtDtTs() {
        return crtDtTs;
    }

    public void setCrtDtTs(Object crtDtTs) {
        this.crtDtTs = crtDtTs;
    }

    public Object getUpDtTs() {
        return upDtTs;
    }

    public void setUpDtTs(Object upDtTs) {
        this.upDtTs = upDtTs;
    }
}
