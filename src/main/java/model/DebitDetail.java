package model;

import java.sql.Blob;

public class DebitDetail {
    private int id_debit ;
    private int id;
    private String note ;
    private int cateria ;
    private float money ;
    private Object crtDtTs;
    private Object upDtTs ;
    private Object image ;


    public DebitDetail(int id, String note, int cateria, float money, Object crtDtTs, Object upDtTs , Object image) {
        this.id = id;
        this.note = note;
        this.cateria = cateria;
        this.money = money;
        this.crtDtTs = crtDtTs;
        this.upDtTs = upDtTs ;
        this.image = image;
    }
    public DebitDetail() {}
    public int getId_debit() {
        return id_debit;
    }

    public void setId_debit(int id_debit) {
        this.id_debit = id_debit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getCateria() {
        return cateria;
    }

    public void setCateria(int cateria) {
        this.cateria = cateria;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public Object getUpDtTs() {
        return upDtTs;
    }

    public void setUpDtTs(Object upDtTs) {
        this.upDtTs = upDtTs;
    }

    public Object getCrtDtTs() {
        return crtDtTs;
    }

    public void setCrtDtTs(Object crtDtTs) {
        this.crtDtTs = crtDtTs;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }
}
