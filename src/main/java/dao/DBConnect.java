package dao;



import java.sql.Connection;
import java.sql.DriverManager;



public class DBConnect {

    public static Connection getcon() {
        Connection con = null ;
        try {
            // Edit URL , username, password to authenticate with your MS SQL Server

            String url = "jdbc:mysql://localhost:3306/fptcredit?useSSL=false";
            String username = "root";
            String password = "123456";

            Class.forName("com.mysql.cj.jdbc.Driver");
             con = DriverManager.getConnection(url, username, password);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
//        YBClusterAwareDataSource ds = new YBClusterAwareDataSource();
//        Connection con = null ;
//        try {
//            // Edit URL , username, password to authenticate with YugabyteDB
//            ds.setURL("jdbc:yugabytedb://ap-east-1.a7393014-0e4b-405f-a49b-04a0ca745978.aws.ybdb.io:5433/yugabyte");
//            ds.setUser("swp490");
//            ds.setPassword("Typingcenter");
//            con = ds.getConnection();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return con;
    }

    public static void main(String[] args) {
        getcon();
    }
}