package dao;

import model.Account;
import tool.MD5;
import tool.Mail;
import tool.Random;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.sun.activation.registries.LogSupport.log;



public class Dao {
    public static boolean insert(Account ac) {


        try {
            Connection myconn = DBConnect.getcon();
            String sql = "insert into account (username,password,active,randomkey,email) values (?,?,?,?,?)";
            String randomkey = Random.randomKey(20);
            try (PreparedStatement mystm = myconn.prepareStatement(sql)) {
                mystm.setString(1, ac.getUsername());
                String password = MD5.getMd5(ac.getPassword());
                mystm.setString(2, password);
                mystm.setInt(3, 0);
                mystm.setString(4, randomkey);
                mystm.setString(5, ac.getEmail());
                mystm.execute();
            }

            Mail.sendMail(ac.getEmail(), "Active tai khoan", Mail.message(ac, randomkey));
            return true;
        } catch (SQLException ex) {
            log("False");
        }
        return false;
    }

    public static boolean checkUsername(String username) throws SQLException {
        Connection con = DBConnect.getcon();
        String query = "SELECT * FROM account where username = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                return true;
            }
        }


        return false;
    }

    public boolean activeUser(String username, String code) throws SQLException {
        Connection con = DBConnect.getcon();
        String sql = "Update account SET active = 1 "
                + "where username = ? and randomkey = ?";
        try (PreparedStatement ls = con.prepareStatement(sql)) {
            ls.setString(1, username);
            ls.setString(2, code);
            return ls.executeUpdate() > 0 ;
        }


    }

    public static Account login(String username, String password) throws SQLException {


        Connection myConn = DBConnect.getcon();


        try {


            String sql = "select * from account where username = ? and password = ?";
            try (PreparedStatement myStmt = myConn.prepareStatement(sql)) {
                String pwrecaptcha = MD5.getMd5(password);
                myStmt.setString(1, username);
                myStmt.setString(2, pwrecaptcha);
                ResultSet rs = myStmt.executeQuery();
                if (rs.next()) {
                    Account ac = new Account();
                    ac.setUsername(rs.getString("username"));
                    ac.setPassword(rs.getString("password"));
                    ac.setEmail(rs.getString("email"));
                    ac.setActive(rs.getInt("active"));


                    return ac;
                }
            }


        } catch (SQLException ex) {
            log("Fail!");
        }

        return null;


    }


    public static boolean login1(String username, String password) throws SQLException {


        Connection myConn = DBConnect.getcon();


        try {


            String sql = "select * from account where username = ? and password = ?";
            try (PreparedStatement myStmt = myConn.prepareStatement(sql)) {
                String pwrecaptcha = MD5.getMd5(password);
                myStmt.setString(1, username);
                myStmt.setString(2, pwrecaptcha);
                ResultSet rs = myStmt.executeQuery();
                if (rs.next()) {
                    Account ac = new Account();
                    ac.setUsername(rs.getString("username"));
                    ac.setPassword(rs.getString("password"));
                    ac.setEmail(rs.getString("email"));
                    ac.setActive(rs.getInt("active"));


                    return true;
                }
            }


        } catch (SQLException ex) {
            log("Fail!");
        }

        return false;


    }

    public Account checkforgot(String username, String email) throws SQLException {


        try {

            Connection myConn = DBConnect.getcon();
            String sql = "select * from account where username = ? and email = ?";
            try (PreparedStatement myStmt = myConn.prepareStatement(sql)) {
                myStmt.setString(1, username);
                myStmt.setString(2, email);
                ResultSet rs = myStmt.executeQuery();
                if (rs.next()) {
                    Account ac = new Account();
                    ac.setUsername(rs.getString("username"));
                    ac.setPassword(rs.getString("password"));
                    ac.setEmail(rs.getString("email"));
                    ac.setActive(rs.getInt("active"));


                    return ac;
                }
            }


        } catch (SQLException ex) {
            log("Fail!");
        }
        return null;


    }

    public boolean sendEmtoForgot(Account ac) throws SQLException {
        Connection con = DBConnect.getcon();
        String randomkey = Random.randomKey(20);
        String sql = "Update account SET randomkey = ? "
                + "where username = ? and email = ?";
        try (PreparedStatement ls = con.prepareStatement(sql)) {
            ls.setString(1, randomkey);
            ls.setString(2, ac.getUsername());
            ls.setString(3, ac.getEmail());
            if (ls.executeUpdate() > 0) {
                Mail.sendMail(ac.getEmail(), "quen mat khau", Mail.messageForgotPs(ac, randomkey));
                return true;
            } else {
                return false;
            }
        }


    }

    public boolean changePassword(String username, String password) throws SQLException {
        Connection con = DBConnect.getcon();
        String sql = "Update account SET password = ? "
                + "where username = ? ";
        try (PreparedStatement ls = con.prepareStatement(sql)) {
            String repassword = MD5.getMd5(password);
            ls.setString(1, repassword);
            ls.setString(2, username);

            return ls.executeUpdate() > 0 ;

        }

    }


}
