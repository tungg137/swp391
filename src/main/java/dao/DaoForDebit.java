package dao;



import model.Account;
import model.Debit;
import model.DebitDetail;
import tool.MD5;
import tool.Mail;
import tool.Random;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.sun.activation.registries.LogSupport.log;

public class DaoForDebit {
    public List<DebitDetail> loadDetail(int id) {
        ArrayList<DebitDetail> list = new ArrayList<>();

        try {

            Connection con = DBConnect.getcon();

            String sql = "select * from debit_detail where id = ?" ;
            try(PreparedStatement ls = con.prepareStatement(sql)){
                ls.setInt(1,id);

                ResultSet rs = ls.executeQuery();
                while (rs.next()) {
                    DebitDetail debit = new DebitDetail();
                    debit.setId_debit(rs.getInt("id_debit"));


                    debit.setId(rs.getInt("id"));

                    debit.setNote(rs.getString("note"));
                    debit.setCateria(rs.getInt("cateria"));
                    debit.setMoney(rs.getFloat("money"));
                    debit.setCrtDtTs(rs.getObject("crtDtTs"));
                    debit.setUpDtTs(rs.getObject("upDtTs"));

                    list.add(debit);
                }






            }
        } catch (SQLException e) {
            log("erro");

        }

        return list;
    }
    public List<Debit> listPeople() {
        ArrayList<Debit> list = new ArrayList<>();

        try {

            Connection con = DBConnect.getcon();
            //"select * from \"Debit\" " ;
            String sql = "select * from debit" ;
            try(PreparedStatement ls = con.prepareStatement(sql)){
                ResultSet rs = ls.executeQuery();
                while (rs.next()) {
                    Debit debit = new Debit();
                    debit.setId(rs.getInt("id"));
                    debit.setName(rs.getString("name"));
                    debit.setAddress(rs.getString("address"));
                    debit.setPhone(rs.getInt("phone"));
                    debit.setEmail(rs.getString("email"));
                    debit.setTotal(rs.getInt("total"));
                    debit.setCrtDtTs(rs.getObject("crtDtTs"));
                    debit.setUpDtTs(rs.getObject("upDtTs"));
                    list.add(debit);
            }






            }
        } catch (SQLException e) {
            log("erro");

        }

        return list;
    }

    public List<DebitDetail> listdetail(int id_debit) {
        ArrayList<DebitDetail> list = new ArrayList<>();

        try {

            Connection con = DBConnect.getcon();
            //"select * from \"Debit\" " ;
            String sql = "select * from debit_detail where id_debit = ?" ;
            try(PreparedStatement ls = con.prepareStatement(sql)){
                ls.setInt(1,id_debit);

                ResultSet rs = ls.executeQuery();
                while (rs.next()) {
                    DebitDetail debit = new DebitDetail();
                    debit.setId_debit(rs.getInt("id_debit"));

                    debit.setNote(rs.getString("note"));
                    debit.setCateria(rs.getInt("cateria"));
                    debit.setMoney(rs.getFloat("money"));
                    debit.setCrtDtTs(rs.getObject("crtDtTs"));
                    list.add(debit);
                }






            }
        } catch (SQLException e) {
            log("erro");

        }

        return list;
    }
    public boolean insert(Debit db) {


        try {
            Connection myconn = DBConnect.getcon();
            String sql = "insert into debit (name, address, phone, email, total,crtDtTs, upDtTs) values (?,?,?,?,?,?,?)";
            try (PreparedStatement mystm = myconn.prepareStatement(sql)) {
                mystm.setString(1, db.getName());
                mystm.setString(2, db.getAddress());
                mystm.setInt(3, db.getPhone());
                mystm.setString(4, db.getEmail());
                mystm.setFloat(5, db.getTotal());
                mystm.setObject(6, db.getCrtDtTs());
                mystm.setObject(7, db.getUpDtTs());


                mystm.execute();
            }

            return true;
        } catch (SQLException ex) {
            log("False");
        }
        return false;
    }

    public boolean updateInfo(Debit db) throws SQLException {
        Connection con = DBConnect.getcon();
        String sql = "Update debit SET name = ?,address = ?,phone = ? ,email = ?,upDtTs=?"
                + " where id = ?";
        try (PreparedStatement ls = con.prepareStatement(sql)) {
            ls.setString(1, db.getName());
            ls.setString(2, db.getAddress());
            ls.setInt(3, db.getPhone());
            ls.setString(4, db.getEmail());
            ls.setObject(5,db.getUpDtTs());
            ls.setInt(6, db.getId());



            return ls.executeUpdate() > 0 ;
        }


    }


    public boolean insertDetail(DebitDetail db) {


        try {
            Connection myconn = DBConnect.getcon();
            String sql = "insert into debit_detail (id,note,cateria,money,crtDtTs,upDtTs,image) values (?,?,?,?,?,?,?)";
            try (PreparedStatement mystm = myconn.prepareStatement(sql)) {
                mystm.setInt(1, db.getId());
                mystm.setString(2, db.getNote());
                mystm.setInt(3, db.getCateria());
                mystm.setFloat(4, db.getMoney());
                mystm.setObject(5, db.getCrtDtTs());
                mystm.setObject(6, db.getUpDtTs());

                mystm.setObject(7, db.getImage());



                mystm.execute();
            }

            return true;
        } catch (SQLException ex) {
            log("False");
        }
        return false;
    }

}
